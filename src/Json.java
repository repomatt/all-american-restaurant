import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class Json {

  public static void main(String[] args) {
    JSONParser parser = new JSONParser();
    try {
      JSONObject restaurant = (JSONObject) parser.parse(new FileReader("res/menu.json"));
      //Bebidas
      JSONArray listaBebida = (JSONArray)restaurant.get("bebidas");
      System.out.println("BEBIDAS");
      for (int i = 0; i<listaBebida.size(); i++){
        JSONObject bebida = (JSONObject) listaBebida.get(i);
        System.out.println("Nombre: "+bebida.get ("nombre"));
        System.out.println("Vendiendo: "+bebida.get("vendiendo"));
        System.out.println("Tamaños: "+bebida.get("tamaño").toString().replace(","," ").replace("[","").replace("]","").replace("\""," "));
        System.out.println();
        System.out.println(bebida.get("precio").toString().replace("},","\n").replace("[","").replace("}]","").replace("{","").replace("\""," "));
        System.out.println();
      }
      System.out.println("HAMBURGUESAS");
      JSONArray listaHamburguesa = (JSONArray)restaurant.get("hamburguesas");
      for (int i = 0; i<listaHamburguesa.size(); i++){
        JSONObject hamburguesa = (JSONObject) listaHamburguesa.get(i);
        System.out.println();
        System.out.println("Nombre: "+hamburguesa.get("nombre"));
        System.out.println("Vendiendo "+hamburguesa.get("vendiendo"));
        System.out.println("Tamaños: "+hamburguesa.get("tamaño").toString().replace(","," ").replace("[","").replace("]","").replace("\""," "));
        System.out.println();
        System.out.println(hamburguesa.get("precio").toString().replace("},","\n").replace("[","").replace("}]","").replace("{","").replace("\""," "));
        System.out.println();
        System.out.print("Ingridientes:");
        System.out.print(hamburguesa.get("ingridientes").toString().replace("},","").replace("[","").replace("]","").replace("{","").replace("\""," "));

        System.out.println();

      }
      JSONArray listaEnsaladas = (JSONArray)restaurant.get("ensaladas");
      System.out.println("ENSALADAS");
      for (int i = 0; i<listaEnsaladas.size(); i++){
        JSONObject ensalada = (JSONObject) listaEnsaladas.get(i);
        System.out.println();
        System.out.println("Nombre: "+ensalada.get("nombre"));
        System.out.println("Vendiendo "+ensalada.get("vendiendo"));
        System.out.println(ensalada.get("precio").toString().replace("}","").replace("[","").replace("}]","").replace("{","").replace("\""," "));
        System.out.print("Ingridientes:");
        System.out.print(ensalada.get("ingridientes").toString().replace("},","").replace("[","").replace("]","").replace("{","").replace("\""," "));

        System.out.println();

      }
      JSONArray listaPostre = (JSONArray)restaurant.get("postre");
      System.out.println("POSTRE");
      for (int i = 0; i<listaPostre.size(); i++){
        JSONObject postre = (JSONObject) listaPostre.get(i);
        System.out.println();
        System.out.println("Nombre: "+postre.get("nombre"));
        System.out.println("Vendiendo "+postre.get("vendiendo"));
        System.out.println(postre.get("precio").toString().replace("}","").replace("[","").replace("}]","").replace("{","").replace("\""," "));
        System.out.print("Ingridientes:");
        System.out.print(postre.get("ingridientes").toString().replace("},","").replace("[","").replace("]","").replace("{","").replace("\""," "));

        System.out.println();

      }
    }catch (Exception e){
      e.printStackTrace();
    }
  }
}
