import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class LolAPI {
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.println("REGION: EU o NA?");

    String region = scan.nextLine() ;
    if(region.equals("EU")){
      region = "euw1";
    }else{
      region = "na1";
    }
    System.out.println("BUSCAR JUGADOR O CONSULTAR LOS RANKS? ");
    String consulta = scan.nextLine();
    String nombre;
    String requestUrl = "";
    String nombreID = "";

    if (consulta.equals("BUSCAR JUGADOR")){
      System.out.println("Nombre: ");
      nombre = scan.nextLine();
      requestUrl = "https://"+region+".api.riotgames.com/lol/summoner/v3/summoners/by-name/"+nombre+"?api_key=RGAPI-eeefc946-bc4f-4dc3-8040-c91f9cd4c2b5";
    }
    if (consulta.equals("CONSULTAR LOS RANKS")){
      System.out.println("El ID: (Si no lo sabes puedes consultar en BUSCAR JUGADOR la id)");
      nombreID = scan.nextLine();
      requestUrl = "https://"+region+".api.riotgames.com/lol/league/v3/positions/by-summoner/"+nombreID+"?api_key=RGAPI-eeefc946-bc4f-4dc3-8040-c91f9cd4c2b5";

    }
    try{
      URL url = new URL(requestUrl);
      HttpURLConnection connection = (HttpURLConnection)url.openConnection();

      connection.setDoInput(true);
      connection.setDoOutput(true);
      connection.setRequestMethod("GET");
      connection.setRequestProperty("Accept","application/json");
      connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
      BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      StringBuffer jsonString = new StringBuffer();
      String line;
      while ((line = br.readLine()) != null){
            jsonString.append(line);
      }
      br.close();
      connection.disconnect();
      System.out.println(jsonString.toString().replace(",","\n").replace("\"","").replace(":",": ").replace("{","").replace("}","").replace("[","").replace("]",""));

    } catch (Exception e){
      e.printStackTrace();
    }
  }
}
