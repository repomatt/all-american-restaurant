import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Scanner;

public class Xml {

  public static void main(@Nullable String[] args) {
    Scanner scan = new Scanner(System.in);

    File XMLFile = new File("res/menu.xml");
    DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
    try {
      DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
      Document xmlDocument = dBuilder.parse(XMLFile);
      xmlDocument.getDocumentElement().normalize();
      System.out.println(xmlDocument.getDocumentElement().getNodeName());

      System.out.println("BEBIDA");
      NodeList nListBebidas = xmlDocument.getElementsByTagName("bebida");

      for (int i = 0; i < nListBebidas.getLength(); i++) {
        Node nodeBebidas = nListBebidas.item(i);
        if (nodeBebidas.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nodeBebidas;
          Element precio = (Element) eElement.getElementsByTagName("precio").item(0);
          System.out.println("Nombre: " + eElement.getElementsByTagName("nombre").item(0).getTextContent());
          System.out.println("Precios: " + eElement.getElementsByTagName("precios").item(0).getTextContent());
          System.out.println("Moneda: "+precio.getAttribute("moneda"));
          System.out.println("Tamaños: " + eElement.getElementsByTagName("tamanos").item(0).getTextContent());
          System.out.println("Vendiendo: " + eElement.getElementsByTagName("vendiendo").item(0).getTextContent());

          System.out.println();

        }
      }
      //Hamburguesas
      NodeList nListHamburguesas = xmlDocument.getElementsByTagName("hamburguesa");
      for (int i = 0; i < nListHamburguesas.getLength(); i++) {
        Node nodeHamburguesas = nListHamburguesas.item(i);
        Element eElement = (Element) nodeHamburguesas;
        Element precio = (Element) eElement.getElementsByTagName("precio").item(0);

        System.out.println("Nombre: " + eElement.getElementsByTagName("nombre").item(0).getTextContent());
        System.out.println("Precio: " + eElement.getElementsByTagName("precios").item(0).getTextContent());
        System.out.println("Moneda: "+precio.getAttribute("moneda"));
        System.out.println("Tamaños: " + eElement.getElementsByTagName("tamanos").item(0).getTextContent());
        System.out.println("Ingredientes: " + eElement.getElementsByTagName("ingredientes").item(0).getTextContent());
        System.out.println("Vendiendo: " + eElement.getElementsByTagName("vendiendo").item(0).getTextContent());
        System.out.println();
      }
      // Ensaladas
      System.out.println("ENSALADAS");
      NodeList nListEnsaladas = xmlDocument.getElementsByTagName("ensalada");
      for (int i = 0; i < nListEnsaladas.getLength(); i++) {
        Node nodeEnsaladas = nListEnsaladas.item(i);
        Element eElement = (Element) nodeEnsaladas;
        Element precio = (Element) eElement.getElementsByTagName("precio").item(0);


        System.out.println("Nombre: " + eElement.getElementsByTagName("nombre").item(0).getTextContent());
        System.out.println("Precio: " + eElement.getElementsByTagName("precio").item(0).getTextContent());
        System.out.println("Moneda: "+precio.getAttribute("moneda"));
        System.out.println("Ingredientes: " + eElement.getElementsByTagName("ingredientes").item(0).getTextContent());
        System.out.println("Vendiendo: " + eElement.getElementsByTagName("vendiendo").item(0).getTextContent());
        System.out.println();
      }
      // postre
      System.out.println("POSTRE");
      NodeList nListPostre = xmlDocument.getElementsByTagName("postre");
      for (int i = 0; i < nListPostre.getLength(); i++) {
        Node nodePostres = nListPostre.item(i);
        Element eElement = (Element) nodePostres;
        Element precio = (Element) eElement.getElementsByTagName("precio").item(0);

        System.out.println("Nombre: " + eElement.getElementsByTagName("nombre").item(0).getTextContent());
        System.out.println("Precio: " + eElement.getElementsByTagName("precio").item(0).getTextContent());
        System.out.println("Moneda: "+precio.getAttribute("moneda"));
        System.out.println("Ingredientes: " + eElement.getElementsByTagName("ingredientes").item(0).getTextContent());
        System.out.println("Vendiendo: " + eElement.getElementsByTagName("vendiendo").item(0).getTextContent());
        System.out.println();
      }


    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
