# All American Restaurant
## Planificar todo
El primero paso era que nosotros nos poníamos de acuerdo como estructuramos el archivo JSON & XML. Hemos pensado hacerlo sobre un restaurante ya que
lo veíamos fácil de rellenar contenido. Al principio teníamos planeado de que uno hace JSON y el otro que haga XML pero hemos descartado
esa opción ya que el objetivo era aprender como crear y consumir información sobre los 2 tipos. Por lo que al final hemos puesto de acuerdo que uno sé encargada
de una estructura por ejemplo que crea los postres y el otro las bebidas etc.
## Creando los 2 archivos
Gracias con el control de versión (git) hemos podido lograr crear los 2 archivos sin estar presente uno al lado del otro. \
![](https://i.imgur.com/jKcTO73.png) ![](https://i.imgur.com/Bi5AKy8.png)
## Consumir la informacion
Lo que nosotros consideramos la parte más difícil (excluyendo el ejercicio API) era consumir los archivos JSON y XML. XML fue bastante sencillo. JSON era un
muy lioso al principio pero una vez te funciona con una consulta, hacer otra era facil. \
![](https://i.imgur.com/AIFY8cX.png) \ ![](https://i.imgur.com/nXwYeDu.png)
# League of Legends API
## Buscando un API
El objetivo nuestro era buscar un API sencillo y fácil de hacer consultas pero nos hemos dado cuenta que aunque la manera de consultar la información es diferente
el proceso de conseguir el API Key eran similares. Hemos decidido a utilizar el API del famoso juego League of Legends. El motivo porque hemos cogido este es
porque había mucha documentación, ejemplos y porque es un juego que jugamos.
## Creando el programa
Antes de esta practica no teníamos mucha idea de que era un API entonces nos hemos puesto a buscar informacion. Hemos utilizado una herramienta que se llama
Postman que nos permite hacer GET requests y que nos muestra el JSON pero con un formato fácil de leer, esto nos daba una idea que como funcionaba, poníamos
el GET y luego el link y dentro de link ponemos lo que queremos buscar y al final el API key. El **El API KEY de League of Legends solo dura aprox. 24 horas.**
 \
![](https://i.imgur.com/HFiX8Wa.png)

## Explicando que consultas hemos elejido
Hemos seleccionado 2 consultas que dependiendo de que pides (si es un BUSCAR JUGADOR o MIRAR LOS RANKS) te retornara información diferente. 
### EU o NA?
Esta pregunta aquí lo que te está pidiendo es si la información que quieres buscar estará en la región de Europa(EU) o en Norteamerica(NA).
### BUSCAR JUGADOR
Básicamente aquí podemos escribir el **nombre** del jugador para ver su nivel, el id y poco más.
Un nombre de ejemplo que podrías utilizar para ver que retorna es **Himself** \
![](https://i.imgur.com/hdcBVGr.png)
### CONSULTAR LOS RANKS
Aquí es como una continuación de BUSCAR JUGADOR porque aquí te muestra toda tus victorias y derrotas y en que división estas. Pero la diferencia aquí es que
te pide el id del jugador. Y para conseguir la ID del jugador tendrías que primero buscarlo en BUSCAR JUGADOR y copiar la id.Puedes utilizar este id 42073667 como ejemplo \
![](https://i.imgur.com/xK9lnXZ.png)
